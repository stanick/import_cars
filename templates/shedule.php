<div class="wrap">
<h2>Расписание</h2>
<ul class="subsubsub">
	<li class="all"><a href="plugins.php?page=import" >Импорт</a> |</li>
	<li class="publish"><a href="plugins.php?page=import&tab=shedule" class="current">Расписание </a></li>
</ul>

<form method="post" enctype="multipart/form-data" style="clear:both">
<table class="wp-list-table widefat fixed striped">
<thead>
	<tr>
		<th class="column-cb check-column"></th>
		<th>Заголовок</th>
		<th>Время выполнения</th>
		<th>Ссылка</th>
	</tr>
	</thead>
<?php foreach($this->getImportSources() as $k=>$v):?>
<tr>
<th  class="check-column"><input type="checkbox" name="check[<?php echo $k?>]" <?php if($this->getSheduledEnabled($k)) echo 'checked';?>></th>
<td><?php echo $v;?></td>
<td>
<select name="next_time[<?php echo $k?>]">
<?php $now=new DateTime();
$hour=$now->format('G');
$now=new DateTime($now->format('Y-m-d').' '.$hour.':00');
for($i=0;$i<24;$i++){
echo '<option value="'.$now->format('U').'"'.(($now->format('U')==$this->getSheduledTime($k))?' selected':'').'>'.$now->format('H:i').'</option>';
$now->add(date_interval_create_from_date_string('1 hour'));
}
?>

</select>
</td>
<td><input type="text" value="<?php echo $this->getSheduledUrl($k)?>" name="sheduleurl[<?php echo $k?>]"></td>
</tr>
<?php endforeach;?>
</table>
<button name="do" value="save">Сохранить</button>
</form>

</div>
