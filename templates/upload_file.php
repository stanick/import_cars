<div class="wrap">
<h2>Выберите источник импорта</h2>
<ul class="subsubsub">
	<li class="all"><a href="plugins.php?page=import" class="current">Импорт</a> |</li>
	<li class="publish"><a href="plugins.php?page=import&tab=shedule" >Расписание </a></li>
</ul>

<form method="post" enctype="multipart/form-data" style="clear:both">
<?php wp_nonce_field( 'import_upload', 'import_upload_nonce' ); ?>
<div>

<select name="import_source">
<?php foreach($this->getImportSources() as $k=>$v):?>
<option value="<?php echo $k?>"><?php echo $v;?></option>
<?php endforeach;?>
</select>
</div>
<div>
<label>Выберите файл:
<input name="file" type="file">
</label>
</div>
<div>
<label>Или просто скопируйте сюда ссылку:
<input type="text" name="url">
</label>
</div>
<div >
<button name="do" value="upload">Загрузить</button> <button name="do" value="delete" onclick="return confirm('Bы уверены, что хотите удалить?')">Удалить все из данного источника</button>
</div>

</form>
</div>
