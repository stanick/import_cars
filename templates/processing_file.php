<div class="wrap">
<h2>Файл успешно загружен, можно начинать импорт</h2>
<div class="progress">
  <div class="progress-bar"style="width: 0%;">
  </div>
</div>
<form class="import_form">
<div >В файле найдено <?php echo $count?> записей</div>
<input type="hidden" name="import_source" value="<?php echo $_GET['import_source']?>">
<input type="hidden" name="action" value="import_cars">
<input type="hidden" name="filename" value="<?php echo ($_GET['filename']);?>">
<input type="hidden" name="import_offset" value="0" class="import_offset">
<label class="start_import">Обновить товары и удалить те которых нет в файле обновления<input type="checkbox" name="refresh"></label>
<button class="start_import">Начать импорт</button>
</form>
<div class="log">

</div>
</div>
<style>
.progress{
	width:100%;
	height:30px;
	background-color: #f5f5f5;
}

.progress .progress-bar{
	height:30px;
	background:#337ab7;
}
.log{
	height:250px;
	overflow:scroll;
}
</style>
<script>
(function($){
var import_limit=1;
var max_items=<?php echo (int)$count;?>;
var affected_ids=[];
function update_dependancies(){
	var data = {'action': 'update_dependancies'};
	$.post(ajaxurl, data, function(response) {
//		$('.log').append(response);
		$('.log').append('<div style="color:#5cb85c">импорт успешно завершен</div>');
	});
	}
function upload_some(){
	//console.log(affected_ids);
	//console.log($('.import_offset').val()+' '+max_items);
	if($('.import_offset').val()>=max_items){
		//console.log($('label.start_import input').is(':checked'));
		if(!$('label.start_import input').prop('checked')){
			update_dependancies();
			return;
		}
		var data = {
			'action': 'delete_cars',
			'ids': affected_ids,
			'import_source':'<?php echo $_GET['import_source'];?>'
		};
		
		$.post(ajaxurl, data, function(response) {
			$('.log').append(response);
			update_dependancies();
			
		});
		
		return;
	}
	$.post(ajaxurl, $('form.import_form').serialize(), function(response) {
			var res=JSON.parse(response);
			//console.log(res.affectedIds);
			affected_ids=affected_ids.concat(res.affectedIds);
			//console.log(affected_ids);
			$('.log').append(res.text);
			$('.import_offset').val(parseInt($('.import_offset').val())+import_limit);
			
			$('.progress .progress-bar').css('width',String($('.import_offset').val()/max_items*100)+'%');
			setTimeout( upload_some, 1000);
			//upload_some();
		});
}
$(document).ready(function(){
	$('form.import_form').submit(function(){
		$('.start_import').hide();
		upload_some();
		return false;
	});
});
})(jQuery)
</script>
