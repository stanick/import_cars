<?php 
require_once('Abstract.php');
class ImportSourceCrwlru extends ImportSourceAbstract{
	public function getcode(){
		return 'crwlru';
	}
	public function count(){
		return(count($this->get_parsed_file()));
	}
	public function get_parsed_file(){
		if(empty($this->parsed_file)){
			$this->parsed_file=json_decode(file_get_contents($this->file),true);
		}
		//var_dump(json_last_error());
		return $this->parsed_file;
	}
	public function map($offer){
		
		$obj=array();

		$id = (string)$offer['Id'];
	if(!empty($offer['latitude'])&&!empty($offer['longitude'])){
		$location =  array(
		        'latitude' => $offer['latitude'],
		        'longitude' => $offer['longitude'],
		        'zoom' => 5,//TODO?
		    ) ;
		}
	else{
		$address = str_replace( ' ', '+', $offer['address'] );
		$location = json_decode( 
		    file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyDt5TJOzYHWWXUtKXw93xo_QXgq7eswvtg&language=ru' ),
		    1
		);
//		var_dump($location);
		if( $location['status'] == 'OK' )
		{
		    $location =  array(
		        'latitude' => $location['results'][0]['geometry']['location']['lat'],
		        'longitude' => $location['results'][0]['geometry']['location']['lng'],
		        'zoom' => 0,
		    ) ;
		} else {
		    $location =  array(
		        'latitude' => 0,
		        'longitude' => 0,
		        'zoom' => 0,
		    ) ;
		}
		}
	//var_dump($location);
        $obj['post_title']=$offer['marka'] . ' ' . $offer['model'] . ' ' . $offer['modification'];
		$obj['post_content']=(string)$offer['info'];
		$obj['comment_status']='open';
		$obj['ping_status']='open';
		$obj['post_status']='publish';
		$obj['post_parent']=0;
		$obj['menu_order']=0;
		$obj['post_type']='listings';
		$metadata=array();
		$metadata['location_address']=$offer['address'];
		$metadata['location_map']=$location;
		$metadata['god-vypuska']=(string)$offer['year'];
		$metadata['marka']=(string)$offer['marka'];
		$metadata['model']=(string)$offer['model'];
		$metadata['dilername']=$offer['fio'];
		$metadata['tip-kuzova']=(string)$offer['body'];
		$metadata['probeg']=(string)$offer['run'];//TODO also may have run ed
		$metadata['korobka-peredach']=(string)$offer['transmission'];
		$metadata['cena']=(string)$offer['price'];
		$metadata['tip-dvigatelya']=(string)$offer['fuel'];
		$metadata['color']=(string)$offer['color'];
//		$metadata['doors']=(string)$offer->doors;//TODO not set may be part of body
		$metadata['drive']=(string)$offer['drive'];
//		$metadata['accident']=(string)$offer->accident;//TODO not set?
		$metadata['condition']=(string)$offer['condition'];// 1 - не битые, 2 - битые
		$metadata['multi_options']= explode( ';', (string)$offer['complect'] ) ;
		$metadata['только_новые']='yes';
		$metadata['car_sold']=2;
		$metadata['listing_options']= serialize(
            array(
                'price' => array(
                    'text' => 'Цена',
                    'value' => (string)$offer['price'],
                    'original' => ''
                ),
                'city_mpg' => array(
                    'text' => 'Город',
                    'value' => (string)$offer['region']
                ),
                'highway_mpg' => array(
                    'text' => 'Шоссе',
                    'value' => ''
                ),
                'badge_text' => '',
                'badge_color' => '',
                'short_desc' => '',
                'video' => '',
            )
         );
		$images=explode(',',$offer['photo']);
		$metadata['source_id']=$id;
		$metadata['source_code']=$this->getcode();
		$obj['metadata']=$metadata;
		$obj['images']=$images;
		return $obj;
	}
	public function getMappedOffer($offset){
		$offer=$this->get_parsed_file()[(int)$offset];
		return $this->map($offer);
	}
	
}
