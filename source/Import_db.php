<?php 
require_once('Abstract.php');
class ImportSourceImport_db extends ImportSourceAbstract{
	public function getcode(){
		return 'import_db';
	}
	public function delete($id){
		global $wpdb;
		$table_import=$wpdb->prefix . "import_data";
		$wpdb->query('delete from '.$table_import.' where id='.(int)$id);
	}
	public function map($offer){
		return json_decode($offer->json_data,true);
			
	}
	public function getUnmappedOffer($icl){
		global $wpdb;
		$table_import=$wpdb->prefix . "import_data";
		
		$wpdb->query('update '.$table_import.' set hold=0 where hold_time<'.(time()-15*60));
		$icl->log('получаем из базы рандомный номер');
		$wpdb->query('START TRANSACTION');
		$row = $wpdb->get_row( 'SELECT * FROM '.$table_import.' where hold=0 limit 1 FOR UPDATE' );
		$icl->log('номер:'.$row->id);
//		var_dump($wpdb->get_row( 'delete FROM '.$table_import.' where json_data=""'));
		if(!empty($row)){
			//getting file and parsing it
			$icl->log('делаем номер холд'.$row->id);
			$wpdb->update( $table_import, array('hold'=>1,'hold_time'=>time()), array('id'=>$row->id), array('%d','%d'), array('%d') );
			$wpdb->query('COMMIT');
			$icl->log('сделали номер холд, загружаем '.$row->id);
			return $row;
			}
		$wpdb->query('ROLLBACK');
		return false;
	}
	
}
