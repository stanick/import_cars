<?php 
require_once('Abstract.php');
class ImportSourceCarcopy extends ImportSourceAbstract{
	public function getcode(){
		return 'carcopy';
	}
	public function count(){
		return($this->get_parsed_file()->offers->offer->count());
	}
	public function get_parsed_file(){
		if(empty($this->parsed_file)){
			$this->parsed_file=new SimpleXMLElement(file_get_contents($this->file));
		}
		return $this->parsed_file;
	}
	public function map($offer){
		$obj=array();

		$id = (string)$offer->id;

        $address = str_replace( ' ', '+', $offer->{'seller-city'} . ', ' . $offer->{'location-address'} );
        $location = json_decode( 
            file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyDt5TJOzYHWWXUtKXw93xo_QXgq7eswvtg&language=ru' ),
            1
        );
        //var_dump(file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyAwb9H8ad0mDH_r2tnMpgYvuS1j_8IDKAI&language=ru' ));
        if( $location['status'] == 'OK' )
        {
            $location =  array(
                'latitude' => $location['results'][0]['geometry']['location']['lat'],
                'longitude' => $location['results'][0]['geometry']['location']['lng'],
                'zoom' => 0,
             );
        } else {
            $location =  array(
                'latitude' => 0,
                'longitude' => 0,
                'zoom' => 0,
            ) ;
        }
        $obj['post_title']=$offer->make . ' ' . $offer->model . ' ' . $offer->version;
		//$obj['guid']='http://stan.r5z.ru/?post_type=listings&#038;p=' . $id;//<guid isPermaLink="false">http://stan.r5z.ru/?post_type=listings&#038;p=' . $id . '</guid>';
		$obj['post_content']=(string)$offer->comment;
		$obj['comment_status']='open';
		$obj['ping_status']='open';
		$obj['post_status']='publish';
		$obj['post_parent']=0;
		$obj['menu_order']=0;
		$obj['post_type']='listings';
		$metadata=array();
		$metadata['location_address']=$offer->{'seller-city'} . ', ' . $offer->{'location-address'};
		$metadata['location_map']=$location;
		$metadata['god-vypuska']=(string)$offer->year;
		$metadata['marka']=(string)$offer->make;
		$metadata['model']=(string)$offer->model;
		$metadata['tip-kuzova']=(string)$offer->{'body-type'};//TODO $newXML .= '            <wp:meta_key>>tip-kuzova</wp:meta_key>';
		$metadata['probeg']=(string)$offer->run;
		$metadata['korobka-peredach']=(string)$offer->transmission;
		$metadata['cena']=(string)$offer->price;
		$metadata['tip-dvigatelya']=(string)$offer->{'engine-type'};
		$metadata['color']=(string)$offer->color;
		$metadata['doors']=(string)$offer->doors;
		$metadata['drive']=(string)$offer->drive;
		$metadata['accident']=(string)$offer->accident;
		$metadata['condition']=(string)$offer->condition;
		$metadata['multi_options']= explode( ', ', (string)$offer->equipment ) ;
		$metadata['только_новые']='yes';
		$metadata['car_sold']=2;
		$metadata['listing_options']= serialize(
            array(
                'price' => array(
                    'text' => 'Цена',
                    'value' => (string)$offer->price,
                    'original' => ''
                ),
                'city_mpg' => array(
                    'text' => 'Город',
                    'value' => (string)$offer->{'seller-city'}
                ),
                'highway_mpg' => array(
                    'text' => 'Шоссе',
                    'value' => ''
                ),
                'badge_text' => '',
                'badge_color' => '',
                'short_desc' => '',
                'video' => '',
            )
        );
		$images=array();
        foreach( $offer->photos->photo AS $photoUrl ){
			$images[]=$offer->photos['root'][0] . $photoUrl;
		}
		$metadata['source_id']=$id;
		$metadata['source_code']=$this->getcode();
		$obj['metadata']=$metadata;
		$obj['images']=$images;
		return $obj;
	}
	public function getMappedOffer($offset){
		$offer=$this->get_parsed_file()->offers->offer[(int)$offset];
		return $this->map($offer);
	}
	
}
