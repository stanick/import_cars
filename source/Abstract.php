<?php

abstract class ImportSourceAbstract{
	/*
	 * setting file for import
	 */
	public function setFile($file){
		$this->file=$file;
	}
	/*
	 * main function for import offer with current offset
	 */
	public function importOffer($offset){
		$offer=$this->getMappedOffer($offset);
		return $this->save($offer);
	}
	/*
	 * function saves associative array into base
	 */
	public function save($offer){

		$rd_args = array(
			'post_status '=>'any',
			'post_type'=>'listings',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'source_id',
					'value' => $offer['metadata']['source_id'],
				),
				
				array(
					'key' => 'source_code',
					'value' => $offer['metadata']['source_code'],
				)
			)
		);
		
		 
		$rd_query = new WP_Query( $rd_args );
		if ( $rd_query->have_posts() ) {
			$rd_query->the_post();
			$offer['ID']=get_the_ID();
			wp_reset_postdata();//TODO needed?
		}
		$metadata=$offer['metadata'];
		//var_dump($metadata);
		$images=$offer['images'];
		unset($offer['metadata']);
		unset($offer['images']);
		if(empty($offer['ID'])){
			$offer['ID'] = wp_insert_post( $offer, false );
		}
		if(empty($offer['ID'])){
			return 'не получилось добавить пост';
		}
		$p=realpath('../wp-content/plugins/automotive/classes/class.listing.php');
		if(empty($p)){
			$p=realpath('../automotive/classes/class.listing.php');
		}
		require_once($p);
		$l=new Listing;
		$listing_categories=get_option('listing_categories');
		foreach($metadata as $meta_key=>$meta_value){
			if(in_array($meta_key,array('color','marka','model','tip-kuzova'))){
				$meta_value=trim($meta_value,' ');
				}

			if(in_array($meta_key,array('color','tip-kuzova','marka','model','korobka-peredach','tip-dvigatelya','dilername'))){
				$terms=$listing_categories[$meta_key]['terms'][$l->slugify($meta_value)]=$meta_value;
				}
			
			update_post_meta($offer['ID'], $meta_key, $meta_value);
		}
		update_option('listing_categories',$listing_categories);
		//detach all images
		
		$args = array(
			'post_parent' => $offer['ID'],
			'post_type'   => 'attachment', 
			'numberposts' => -1,
			'post_status' => 'any' 
		); 
		$old_images = get_children( $args, $output ); 
		foreach ( $old_images as $attachment_id => $attachment ) {
			$my_post = array(
				'ID'           => $attachment_id,
				'post_parent' => 0,
			);
			wp_update_post( $my_post);
		}


		$attach_ids=array();
		if( !class_exists( 'WP_Http' ) )
			require_once( ABSPATH . WPINC. '/class-http.php' );
		foreach($images as $url){
			if(empty($url)) continue;
			$photo = new WP_Http();
			$photo = $photo->request( $url );
			if(is_wp_error($photo)){
				continue;//TODO log?
				}
			$attachment = wp_upload_bits( base64_encode($url) . '.jpg', null, $photo['body'], date("Y-m", strtotime( $photo['headers']['last-modified'] ) ) );//TODO плохая штука, мы не знаем формат файла, можем просто поверить сайту
			$filetype = wp_check_filetype( basename( $attachment['file'] ), null );
			
			$postinfo = array(
				'post_mime_type'	=> $filetype['type'],
				'post_title'		=> $offer['post_title'],
				'post_content'	=> '',
				'post_status'	=> 'inherit',
			);
			$filename = $attachment['file'];
			$attach_ids[] = wp_insert_attachment( $postinfo, $filename, $offer['ID'] );
			//var_dump($attach_ids);
		}
		//var_dump($attach_ids);
		update_post_meta($offer['ID'], 'gallery_images',  $attach_ids );
		return array('text'=>'<div>Успешно испортирован:'.$offer['post_title'].'</div>','id'=>$offer['ID']);
	}
	
}
