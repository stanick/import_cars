<?php 
require_once('Abstract.php');
class ImportSourceSimplecsv extends ImportSourceAbstract{
	public function getcode(){
		return 'simplecsv';
	}
	public function count(){
		$row=0;
		if (($handle = fopen($this->file, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
			$row++;
			}
		}
		
		fclose($handle);
		return $row;
	}
	public function map($offer){
		$obj=array();
		
		$id = $offer[0];
		$metadata=array();
		$images=array();
		$metadata['>tip-kuzova']=(string)$offer[2];
        $metadata['source_id']=$id;
		$metadata['source_code']=$this->getcode();
		$images=explode("\r\n",$offer[4]);
		$obj['metadata']=$metadata;
		$obj['images']=$images;
		return $obj;
	}
	public function getMappedOffer($offset){
		$row=0;//TODO 1 if has headers
		if (($handle = fopen($this->file, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
			if($row==$offset){
				$offer=$data;
				break;
			}
			$row++;
			}
		}
		
		fclose($handle);
		return $this->map($offer);
	}
	
}