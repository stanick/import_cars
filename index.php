<?php
/*
Plugin Name: Import
Description: Плагин для импорта машин
Version: 1.0
Author: GoodSellUs
Author URI: http://goodsellus.com/
*/
define('CRWLRU_APIKEY','3977448f33e4a228f8ee8db554e98546');
new ImportClass();

class ImportClass {

	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		add_action('admin_menu', array($this,'add_menu'));
		add_filter( 'upload_mimes', array($this,'additional_mime_types') );
		add_action( 'wp_ajax_import_cars', array($this,'import_some_offers') );
		add_action( 'wp_ajax_delete_cars', array($this,'delete_some_cars') );
		add_action( 'wp_ajax_update_dependancies', array($this,'update_dependancies'));
		$this->db_register();
	}
	/**
	 * adding menu
	 */
	public function add_menu(){
		$this->importpage=add_plugins_page( 'Импорт', 'Импорт','read','import',array($this,'importpage'));
	}
	/*
	 * adding mime types for uploading files
	 */
	function additional_mime_types( $mimes ) {
		$mimes['xml'] = 'text/xml';
		$mimes['csv'] = 'text/csv';
		$mimes['json']= 'application/json';
		return $mimes;
	}
	/*
	 * returns sources,that we can import
	 */
	public function getImportSources(){
		return array('carcopy'=>'загрузка из xml carcopy','simplecsv'=>'Тестовый csv','crwlru'=>'загрузка из crwl.ru. в поле для ссылки укажите количество часов');	
	}
	/*
	 * returns class for source key
	 */
	public function getImportSource($key){
		require_once('source/'.ucwords($key).'.php');
		$className='ImportSource'.ucwords($key);
	return new $className;
	}
	/*
	 * registers our table in db
	 */
	public function db_register(){
		global $wpdb;
		$table_name = $wpdb->prefix . "import_shedule";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		
		$sql = "CREATE TABLE " . $table_name . " (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			enabled tinyint(1) NOT NULL,
			trying tinyint(1),
			trying_time int(11),
			next_time bigint(11) DEFAULT '0' NOT NULL,
			source VARCHAR(20) NOT NULL,
			url text NOT NULL,
			UNIQUE KEY id (id)
			);";
		
		dbDelta($sql);
		$table_import=$wpdb->prefix . "import_data";
		$sql = "CREATE TABLE " . $table_import . " (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			hold tinyint(1) NOT NULL,
			hold_time int(11),
			json_data text NOT NULL,
			UNIQUE KEY id (id)
			);";
		
		dbDelta($sql);
	}
	/*
	 * main page of plugin
	 */
	public function importpage($args){
		if($_GET['tab']=='shedule'){
			if($_POST['do']=='save'){
				//сохраняем 
				global $wpdb;
				$table_name = $wpdb->get_blog_prefix() . 'import_shedule';
	
				foreach($_POST['next_time'] as $k=>$v){
					$wpdb->delete($table_name, array('source'=>$k),array( '%s' ));
					$wpdb->insert( 
								$table_name, 
								array( 
									'source' => $k, 
									'enabled' => !empty($_POST['check'][$k]),
									'next_time' => $v,
									'url' => $_POST['sheduleurl'][$k],
									
								), 
								array( 
									'%s', 
									'%d',
									'%d',
									'%s',
								) 
							);
					}
				}
			include('templates/shedule.php');
			return;	
		}
		if(!empty($_GET['filename'])){
			$file=base64_decode($_GET['filename']);
			if(!empty($file)&&file_exists($file)){
				$so=$this->getImportSource($_GET['import_source']);
				$so->setFile($file);
				$count=$so->count();
				include('templates/processing_file.php');
				return;
			}
		}
		if($_POST['do']=='upload'){
			$fileurl=$_POST['url'];
			if($_POST['import_source']=='crwlru'){
				$fileurl='http://crwl.ru/api/rest/latest/get_ads/?api_key='.CRWLRU_APIKEY;
				$fileurl='http://crwl.ru/api/unload/dealers/?api_key=783d5672a3ad27cfb5e304cc87c746a3';
				if(!empty($_POST['url'])&&is_numeric($_POST['url'])){
					$fileurl.='&last='.(int)$_POST['url'];
					}
				}
			if(isset( $_POST['import_upload_nonce'])&& wp_verify_nonce( $_POST['import_upload_nonce'], 'import_upload' )){
				if(!empty($fileurl)){
					$file = new WP_Http();
					$file = $file->request( $fileurl,array('timeout'=>0));
//					var_dump($file);
					$content_type_to_extension=array('application/xml'=>'.xml','text/csv'=>'.csv','application/json'=>'.json');
					$attachment = wp_upload_bits( base64_encode($fileurl).$content_type_to_extension[$file['headers']['content-type']],null,$file['body']);
					//var_dump($attachment);
					$arr=array('page'=>'import','filename'=>base64_encode($attachment['file']),'import_source'=>$_POST['import_source']);
					wp_redirect('plugins.php?'.http_build_query($arr));
				}
				else{
					$file=wp_handle_upload($_FILES['file'],array( 'test_form' => false ));
					if(isset($file)&&empty($file['error'])){
						$arr=array('page'=>'import','filename'=>base64_encode($file['file']),'import_source'=>$_POST['import_source']);
						wp_redirect('plugins.php?'.http_build_query($arr));
						}
					else{
						var_dump($file['error']);
					}
				}
				return;
			}
		}
		elseif($_POST['do']=='delete'){
			$rd_args = array(
				'post_status '=>'any',
				'post_type'=>'listings',
				'nopaging'=>true,
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'source_code',
						'value' => $_POST['import_source'],
						)
					)
				);
			$rd_query = new WP_Query( $rd_args );
			if ( $rd_query->have_posts() ) {
				while ( $rd_query->have_posts() ) {
					$rd_query->the_post();
					$id=get_the_ID();
					wp_delete_post( $id,true );
					}
				wp_reset_postdata();//TODO needed?
				}
			$this->update_dependancies();
			echo 'Все удачно удалено';
			}
		include('templates/upload_file.php');
	}
	/*
	 * import some offers and returns response for admin-ajax
	 */

	function import_some_offers() {
		$result=array('text'=>'','affectedIds'=>array());
		$so=$this->getImportSource($_POST['import_source']);
		$so->setFile(base64_decode($_POST['filename']));
		
		
		
		$res=$so->importOffer($_POST['import_offset']);
		$result['text'].=$res['text'];
		$result['affectedIds'][]=$res['id'];
		
		
		
		
		echo json_encode($result);
		wp_die(); // this is required to terminate immediately and return a proper response
	}
	/*
	 * delete allcars from current source exept updated ones
	 */
	public function delete_some_cars($ids=array(),$source=''){
		if(empty($ids)&&!empty($_POST['ids'])){
			$ids=$_POST['ids']	;
			}
		if(empty($source)&&!empty($_POST['imoprt_source'])){
			$source=$_POST['import_source'];
			}
		$rd_args = array(
			'post_status '=>'any',
			'post_type'=>'listings',
			'post__not_in' => $ids,
			'nopaging'=>true,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'source_code',
					'value' => $source,
				)
			)
		);
		 
		$rd_query = new WP_Query( $rd_args );
		if ( $rd_query->have_posts() ) {
			while ( $rd_query->have_posts() ) {
				$rd_query->the_post();
				$id=get_the_ID();
				wp_delete_post( $id,true );
			}
			wp_reset_postdata();//TODO needed?
		}
		echo 'Незатронутые данные удалены';
		//wp_die(); // this is required to terminate immediately and return a proper response
	}
	public function update_dependancies(){
		$p=realpath('../wp-content/plugins/automotive/classes/class.listing.php');
		if(empty($p)){
			$p=realpath('../automotive/classes/class.listing.php');
		}
		require_once($p);
		$l=new Listing;
		$l->generate_dependancy_option(true);
		}
	public function getSheduledArray(){
		if(!empty($this->sheduled_arr)){
			return $this->sheduled_arr;
			}
		global $wpdb;
		$table_name = $wpdb->get_blog_prefix() . 'import_shedule';
		$results = $wpdb->get_results( 'SELECT * FROM '.$table_name, OBJECT );
		foreach($results as $res){
			$this->sheduled_array[$res->source]=array(
				'next_time'=>$res->next_time,
				'url'=>$res->url,
				'enabled'=>$res->enabled,
				);
			}
		//$product = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$table_name} WHERE `id` = %d LIMIT 1;", $product_id ) );
		return $this->sheduled_array;
		}
	public function getSheduledEnabled($source){
		$arr=$this->getSheduledArray();
		return $arr[$source]['enabled'];
		}
	public function getSheduledTime($source){
		$arr=$this->getSheduledArray();
		return $arr[$source]['next_time'];
		}
	public function getSheduledUrl($source){
		$arr=$this->getSheduledArray();
		return $arr[$source]['url'];
		
		}
	public function key(){
		if(empty($this->_key)){
		    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 10; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    $this->_key=$randomString;
	    }
	    
	    return $this->_key;
		}
	public function left_import(){
		global $wpdb;
		$table_import=$wpdb->prefix . "import_data";
		$row=$wpdb->get_row( 'SELECT count(*)as cnt FROM '.$table_import );
		return $row->cnt;
		}
	public function log($message){
		$text=date("Y-m-d H:i:s").' '.$this->left_import().' '.$this->key().' - '.$message."\n";
		file_put_contents('log.txt',$text,FILE_APPEND);
		}
}
